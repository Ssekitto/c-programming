#include <stdio.h>
#include <math.h>

int main() {
    float a, b, c, d, e, f, g;

    printf("Enter the coefficient of x^2\n");
    scanf("%f", &a);

    while (a == 0) {
        printf("Enter another value for the coefficient of x^2 (a cannot be zero)\n");
        scanf("%f", &a);
    }

    printf("Enter the coefficient of x\n");
    scanf("%f", &b);

    printf("Enter the value of the constant\n");
    scanf("%f", &c);

    d = (b * b) - (4 * a * c);
    e = sqrt(fabs(d));  // Use fabs to ensure a positive value for the square root

    printf("The square root of %.2f is %.2f\n", d, e);

    if (d < 0) {
        printf("The roots are complex\n");
        f = -b / (2 * a);
        g = e / (2 * a);

        // Print the complex roots
        printf("The roots of the equation %.2fx^2 + %.2fx + %.2f are: %.2f + %.2fi and %.2f - %.2fi\n", a, b, c, f, g, f, g);
    } else if (d > 0) {
        f = (-b + e) / (2 * a);
        g = (-b - e) / (2 * a);
        printf("The roots of the equation %.2fx^2 + %.2fx + %.2f are: %.2f and %.2f\n", a, b, c, f, g);
    } else if (d == 0) {
        printf("The roots are the same\n");
        f = -b / (2 * a);
        printf("The roots of the equation %.2fx^2 + %.2fx + %.2f are: %.2f\n", a, b, c, f);
    }

    return 0;
}
